import React, { useState } from "react";
import Modal from "./MyModal";
import {
  List,
  ListItem,
  ListItemText,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import db from "./firebase";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  todo__list: {
    display: "flex",
    flexWrap: "wrap",
    backgroundColor: "rgba(211,211,211,0.35)",
    borderRadius: "10px",
  },
}));

function Todo(props) {
  const classes = useStyles();

  const [open, setOpen] = useState(false);
  const deleteTodo = () => {
    db.collection("todos").doc(props.todo.id).delete();
  };

  return (
    <>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        bottonName="Edit"
        todo={props.todo}
        setOpen={setOpen}
      />

      <List>
        <ListItem className={classes.todo__list}>
          <ListItemText primary={props.todo.todo} className={classes.todo} />

          <Tooltip title="Edit">
            <IconButton onClick={(e) => setOpen(true)}>
              <EditIcon className={classes.editIcon} color="primary" />
            </IconButton>
          </Tooltip>

          <Tooltip title="Delete">
            <IconButton onClick={deleteTodo}>
              <DeleteIcon color="secondary" />
            </IconButton>
          </Tooltip>
        </ListItem>
      </List>
    </>
  );
}

export default Todo;
