import React, { useState, useEffect, Fragment } from "react";
import Button from "@material-ui/core/Button";
import db from "./firebase";
import "./App.css";
import { TextField } from "@material-ui/core";
import Todo from "./Todo";
import firebase from "firebase";
import Header from "./Header";
import { makeStyles } from "@material-ui/core/styles";
import Footer from "./Footer";

const useStyles = makeStyles((theme) => ({
  app__textField: {
    flex: 1,
    minWidth: "75px",
  },
}));

function App() {
  const classes = useStyles();
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    console.log('inside useEffect');
    db.collection("todos")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) => {
        console.log('inside snapshot')
        setTodos(
          snapshot.docs.map((doc) => ({ todo: doc.data().todo, id: doc.id }))
        );
      });
    
  }, []);

  const handleChange = (event) => {
    setInput(event.target.value);
  };

  const addTodo = (event) => {
    event.preventDefault();
    db.collection("todos").add({
      todo: input,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    setInput("");
  };

  return (
    <div>
      <div className="app">
        <Header />

        {/* Random Quote Generator */}
        {/* Add Todo Component */}
        <form className="app__form">
          <TextField
            id="outlined-basic"
            label="Write a Todo"
            variant="outlined"
            className={classes.app__textField}
            type="text"
            value={input}
            onChange={handleChange}
          />
          <Button
            className={classes.app__addButton}
            type="submit"
            onClick={addTodo}
            variant="contained"
            color="primary"
            disabled={!input}
          >
            ADD
          </Button>
        </form>
        {/* Dropdown menu * 2 */}
        {/* TODO List */}
        {console.log("todos>>",todos)}
        <div className="app__todo">
          {todos.map((todo, i) => (
            <Todo key={i} todo={todo} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
