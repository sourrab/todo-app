import React from "react";

function Header() {
  return (
    <div style={{ display: "flex", marginLeft: "10%" }}>
      <img
        style={{ objectFit: "contain", maxWidth: "400px", width: "100%" }}
        src="/todo.png"
        alt="todo_image"
      />
    </div>
  );
}

export default Header;
