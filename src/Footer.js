import React from "react";
import CopyrightIcon from "@material-ui/icons/Copyright";

function Footer() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        gap: "10px",
        position: "fixed",
        bottom: "5px",
        left: 0,
        right: 0,
      }}
    >
      <CopyrightIcon />
      <span> Saurab Ghimire</span>
    </div>
  );
}

export default Footer;
