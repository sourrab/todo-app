import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Modal, Button } from "@material-ui/core";
import db from "./firebase";
import firebase from "firebase";

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "relative",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  todo: {
    padding: "10px",
    marginRight: "5px",
  },
}));

function MyModal({ open, onClose, bottonName, todo, setOpen }) {
  const [modalStyle] = React.useState(getModalStyle);
  const classes = useStyles();
  const [inputModal, setInputModal] = useState("");
  const updateTodo = () => {
    db.collection("todos").doc(todo.id).set({
      todo: inputModal,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    console.log("input", inputModal);
    setInputModal("");
    setOpen(false);
  };
  useEffect(() => {
    setInputModal(todo.todo);
  }, [todo.todo]);

  return (
    <Modal open={open} onClose={onClose}>
      <div style={modalStyle} className={classes.paper}>
        <h1>Edit Todo</h1>
        <form>
          <input
            className={classes.todo}
            value={inputModal}
            onChange={(e) => {
              setInputModal(e.target.value);
            }}
          />
          <Button
            disabled={!inputModal}
            type="submit"
            onClick={updateTodo}
            variant="contained"
            color="primary"
          >
            {bottonName}
          </Button>
        </form>
      </div>
    </Modal>
  );
}

export default MyModal;
