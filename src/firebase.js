import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBpjT2xgun8nm82NaaEfy8_1fTCqqEXAwU",
  authDomain: "todo-e82a1.firebaseapp.com",
  databaseURL: "https://todo-e82a1.firebaseio.com",
  projectId: "todo-e82a1",
  storageBucket: "todo-e82a1.appspot.com",
  messagingSenderId: "23426576342",
  appId: "1:23426576342:web:bf9dfce27b03e206bbfe77",
  measurementId: "G-5S8KPV8JDC",
});

const db = firebaseApp.firestore();

export default db;
